﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AddressApp
{
    class Menu : IMenu
    {
        ContactList list = new ContactList();

        public void StarMenu() {
            bool flag=true;
            int input;
            Console.WriteLine("******* WELCOME TO ADDRESSBOOK********");
            do
            {
                Console.WriteLine("Please Choose Any Option");
                Console.WriteLine("1.Add Contact \n2.Delete Contact \n3. Edit Contact \n4. List All \n5.Search Contact 6. Exit");
                var str = Console.ReadLine();
                var con=int.TryParse(str, out input);
               // input = Convert.ToInt32( Console.ReadLine());
                if (con && input > 0 && input < 7)
                    flag = false;
                else
                {
                    Console.WriteLine("You choose a wrong choice");
                    Console.Clear();

                }
            } while (flag);


            switch (input)
            {
                case 1:
                    AddItemMenu();
                    break;
                case 2:
                    DeleteItemMenu();
                    break;
                case 3:
                    EditItemMenu();
                    break;
                case 4:
                    ListAllMenu();
                    break;
                case 5:
                    SearchMenu();
                    break;
                case 6:
                    Environment.Exit(0);
                    break;


            }



        }
        public void AddItemMenu() {
            ConsoleKey response;
            do
            {
                ContactModel model = new ContactModel();
                Console.Write("\nEnter Name: ");
              //  model.Name = Console.ReadLine();
                model.Name = ReadData("name");
                Console.Write("Enter Email: ");
                model.Email = ReadEmail();
                Console.Write("Enter Phone: ");
                model.Phone = ReadData("phone");
                int total=list.AddContact(model);
                Console.WriteLine("Total contacts:" + total);
                Console.WriteLine("Do you want to Add Another: ( Y/N)");
                response = Console.ReadKey().Key;
            }
            while (response == ConsoleKey.Y );

        
        }
        public void DeleteItemMenu(){
            Console.Write("Enter Email to delete Record: ");
            var email = Console.ReadLine();

            bool flag=list.DeleteContact(email);
            if (flag)
                Console.WriteLine("Record successfully deleted");

        }
        public void EditItemMenu() {
            Console.Write("Enter Email to Edit Record: ");
            var email = Console.ReadLine();
            IContactModel oldData = list.SearchFirst(email);
            IContactModel model = new ContactModel();
            Console.Write("\nEnter Name:");
            EditLiine(oldData.Name);
         //   model.Name = Console.ReadLine();
            model.Name = ReadLine(oldData.Name);
            Console.Write("Enter Email: ");
            EditLiine(oldData.Email);
            model.Email = ReadLine(oldData.Email);
            Console.Write("Enter Phone:");
            EditLiine(oldData.Phone);
            model.Phone = ReadLine(oldData.Phone);
           // Console.WriteLine("{0}. \tName:{1},\tEmail:{2}\tPhone: {3}  ", 1, model.Name, model.Email, model.Phone);
            list.EditContact(model, oldData);
            printMenu(list.ListAll());

        }

        public string ReadEmail()
        {
            while (true)
            {
                var email = Console.ReadLine();
                bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (isEmail)
                {

                    IContactModel cm = list.SearchFirst(email);
                    if (cm == null)
                        return email;
                    else
                    {
                        Console.WriteLine("Email already used, try another");
                        Console.Write("Enter Email: ");
                    }
                }
                else
                {
                    Console.WriteLine("Email not correct, try again");
                    Console.Write("Enter Email: ");
                }
            
            }

        }


        public string ReadData(string key)
        {
            string name = "";
            while (true)
            {
             //   var input = Console.ReadKey();

                ConsoleKey response = Console.ReadKey().Key;
                if (key=="name" && response >= ConsoleKey.A && response <= ConsoleKey.Z)
                    name += response;
                else if (key == "phone" && response >= ConsoleKey.D0 && response <= ConsoleKey.D9)
                {
                    name += response;
                }

                else if (response == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }
                else
                {
                    Console.Write("\b \b");

                }
                    


            }
            return name;

        }


        public string ReadLine(string oldVal)
        {
           

            var newVal = Console.ReadLine();

            if (newVal.Length >= oldVal.Length)
                return newVal;
            else
            {
                newVal += oldVal.Substring(newVal.Length);
                return newVal;
            }


        }

        public void EditLiine(string data)
        {
            string backup = new string('\b', data.Length);

            var line = string.Format("{0}", data);
            Console.Write(line);
            Console.Write(backup);


        }

        public void ListAllMenu()
        {
           
            List<IContactModel> contacts=list.ListAll();
            printMenu(contacts);
           
        }

        void printMenu(List<IContactModel> contacts)
        {
            int i = 1;
            foreach (IContactModel model in contacts)
            {
                Console.WriteLine("{0}. \tName:{1},\tEmail:{2}\tPhone: {3}  ", i++, model.Name, model.Email, model.Phone);
                if (i % 5 == 0)
                {
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                    Console.Clear();

                }


            }

        }

        public void SearchMenu() {

            Console.Write("Enter term to search");
            var data = Console.ReadLine();
            var contacts=list.SearchContact(data);
            printMenu(contacts);
        }

        public void repeatMenu()
        {
            bool flag=false;
            do
            {
                Console.WriteLine("Do you want to continue?(Y/N)");
                ConsoleKey response = Console.ReadKey().Key;
                if (response == ConsoleKey.Y)
                {

                    StarMenu();
                    flag = true;
                }
                else
                {
                    flag = false;


                }
            }
            while (flag);
            list.WriteList();
           //  Environment.Exit(0);

        }
    }
}
